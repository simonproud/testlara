@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			<div class="comments">
			<h3 class="title-comments" id="title-comments">Комментарии (<span>{{$count}}</span>)</h3>
			<ul class="media-list" id="comments-list">
			  <!-- Комментарий (уровень 1) -->
			  @if($rootcom != 0 && isset($rootcom))
			  @foreach($rootcom as $comment)
			  
				<li class="media">
				<div class="media-body">
				  <div class="media-heading">
					<div class="author">{{$comment['user_name']}}</div>
					<div class="metadata">
					  <span class="date">{{$comment['date']}}</span>
					</div>
				  </div>
				  <div class="media-text text-justify">{{$comment['comment']}}<br><br><a href="{{$comment['filepath']}}"><img src="{{$comment['filepath']}}"></a></div>
				  <div class="footer-comment">
					<span class="vote plus" title="Нравится">
					  <i class="fa fa-thumbs-up"></i>
					</span>       
					<span class="rating">
					  +1 
					</span>
					<span class="vote minus" title="Не нравится">
					  <i class="fa fa-thumbs-down"></i>
					</span>
					<span class="devide">
					 |
					</span>
					<span class="comment-reply">
					 <a href="#" class="reply" data-id="{{$comment['id']}}" data-name="{{$comment['user_name']}}">ответить</a>
					</span>
				  </div>
						@include('comments.comment', ['parent_id' => $comment['id'], 'comments' => $comments])
				</div>
			</li>
			
			  @endforeach
			  @endif
			</ul>
			</div>
			<div id="pagination">
			@if(isset($_REQUEST['p']) && $_REQUEST['p'] >= 4)
				<a href="?p=1">1</a>
				<a href="?p=<?=$_REQUEST['p'] - 2?>"><?=$_REQUEST['p'] - 2?></a>
				<a href="?p=<?=$_REQUEST['p'] - 1?>"><?=$_REQUEST['p'] - 1?></a>
				<a href="#"><?=$_REQUEST['p']?></a>
				<a href="?p=<?=$_REQUEST['p'] + 1?>"><?=$_REQUEST['p'] + 1?></a>
				<a href="?p=<?=$_REQUEST['p'] + 2?>"><?=$_REQUEST['p'] + 2?></a>
			@else
				<a href="?p=1">1</a>
				<a href="?p=2">2</a>
				<a href="?p=3">3</a>
				<a href="?p=4">4</a>
				<a href="?p=5">5</a>
				<a href="?p=6">6</a>
			@endif
			</div>
			<div id="div-sendcomment">
				<form method="POST" id="comment-form" enctype="multipart/form-data">
				 @csrf
					
					<div class="form-group">
						<label for="comment-text">Комментарий</label>
						<textarea class="form-control" id="comment-text" name="comment" rows="3"></textarea>
					</div>
					<div class="form-group"  id="file-group">
						<label for="comment-text">Файл</label>
						<input type="file" id="picture" class="form-control col-md-6" data-base64="null" name="efile" >
					</div>
					<div class="form-group " id="parent-group" style="display:none;">
						<input type="text" id="parent-text" class="form-control col-md-3" value="" name="parent-text" disabled style="float:left">&nbsp;&nbsp;<a href="#" id="clearParent" class="badge badge-danger" >X</a>
						<input type="hidden" id="parent" class="form-control col-md-3" name="parent">
					</div>
					
					<input class="btn btn-primary" type="submit" id="submit-comment" value="Отправить">
				</form>
				<div id="ajaxResponse"></div>
			</div>
        </div>
    </div>
</div>
@endsection
