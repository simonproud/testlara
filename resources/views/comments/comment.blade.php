			@if($comments)
			@foreach($comments as $comment)
			@if($parent_id == $comment['parent_id'])
				<div class="media">
				<div class="media-body">
				  <div class="media-heading">
					<div class="author">{{$comment['user_name']}}</div>
					<div class="metadata">
					  <span class="date">{{$comment['date']}}</span>
					</div>
				  </div>
				  <div class="media-text text-justify">{{$comment['comment']}}<br><br><a href="{{$comment['filepath']}}"><img src="{{$comment['filepath']}}"></a></div>
				  <div class="footer-comment">
					<span class="vote plus" title="Нравится">
					  <i class="fa fa-thumbs-up"></i>
					</span>       
					<span class="rating">
					  +1 
					</span>
					<span class="vote minus" title="Не нравится">
					  <i class="fa fa-thumbs-down"></i>
					</span>
					<span class="devide">
					 |
					</span>
					<span class="comment-reply">
					 <a href="#" class="reply" data-id="{{$comment['id']}}" data-name="{{$comment['user_name']}}">ответить</a>
					</span>
					@include('comments.comment', ['parent_id' => $comment['id'], 'comments' => $comments])
				  </div>
				</div>
				
			</div>
			@endif
			  @endforeach
			  @endif