$(document).ready(function(){
	var activecomment;
	$('#clearParent').click(function(event){
		event.preventDefault();
		$('#parent-text').val("");
		$('#parent').val("");
		$('#parent-group').hide();
	});
	
	$('.reply').click(function(event){
		event.preventDefault();
		$('#parent-group').show();
		$('#parent-text').val($(this).attr('data-name'));
		$('#parent').val($(this).attr('data-id'));
		activecomment = $(this);
	});
	
	function getBase64(file) {
	   var reader = new FileReader();
	   reader.readAsDataURL(file);
	   reader.onload = function () {
		 $('#picture').attr('data-base64', reader.result);
	   };
	   reader.onerror = function (error) {
		 console.log('Error: ', error);
	   };
	   return reader.result;
	}
	$('#file-group').on('change', '#picture', function(){
		getBase64($(this).prop('files')[0]);
	});
	$('#submit-comment').on('click', function (event) {
        event.preventDefault();
		data = {
			'comment': $('#comment-text').val(),
			'efile': $('#picture').attr('data-base64'),
			'parent-text': $('#parent-text').val(),
			'parent': $('#parent').val()
		};

        $.ajax({
			type:'POST',
			enctype: 'multipart/form-data',
            data: data,
            url: '/home',
            dataType: 'json',
			headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function( msg ) {
				if(msg.status == 'success'){
					$('#parent-text').val("");
					$('#parent').val("");
					$('#comment-text').val("");
					$('#parent-group').hide();
					var countcom = parseInt($('#title-comments span').text());
					
					countcom++;
					
					$('#title-comments span').text(countcom);
					$("#ajaxResponse").html("<div>"+msg.msg+"</div>");
					if(activecomment != undefined){
					var comment = '<div class="media"><div class="media-body"><div class="media-heading"><div class="author">'+msg.user+'</div> <div class="metadata"><span class="date">'+msg.comment.updated_at+'</span></div></div> <div class="media-text text-justify">'+msg.comment.comment+'</div> <div class="footer-comment"><span title="Нравится" class="vote plus"><i class="fa fa-thumbs-up"></i></span> <span class="rating">					  +1 					</span> <span title="Не нравится" class="vote minus"><i class="fa fa-thumbs-down"></i></span> <span class="devide">					 |					</span> <span class="comment-reply"><a href="#" data-id="'+msg.comment.id+'" data-name="'+msg.user+'" class="reply">ответить</a></span></div></div></div>';
					$(activecomment[0]).closest('.media-body').append(comment);
					}
					else{
					var comment = '<li class="media"><div class="media-body"><div class="media-heading"><div class="author">'+msg.user+'</div> <div class="metadata"><span class="date">'+msg.comment.updated_at+'</span></div></div> <div class="media-text text-justify">'+msg.comment.comment+'</div> <div class="footer-comment"><span title="Нравится" class="vote plus"><i class="fa fa-thumbs-up"></i></span> <span class="rating">					  +1 					</span> <span title="Не нравится" class="vote minus"><i class="fa fa-thumbs-down"></i></span> <span class="devide">					 |					</span> <span class="comment-reply"><a href="#" data-id="'+msg.comment.id+'" data-name="'+msg.user+'" class="reply">ответить</a></span></div></div></li>';
						$('#comments-list').append(comment);
					}
						$('.reply').click(function(event){
							event.preventDefault();
							$('#parent-group').show();
							$('#parent-text').val($(this).attr('data-name'));
							$('#parent').val($(this).attr('data-id'));
							activecomment = $(this);
						});
					
				}
				if(msg.status == 'error'){
					alert(msg.error);
				}
				console.log(msg);
            }
        });
    });
});