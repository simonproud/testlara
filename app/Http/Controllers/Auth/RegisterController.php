<?php

namespace App\Http\Controllers\Auth;

use Mail;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'sex' => 'required|boolean|max:1',
            'birthday' => 'required|date|max:12',
            
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
		$passstr = str_random(8);
		$password = Hash::make($passstr);
		
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'sex' => $data['sex'],
            'birthday' => $data['birthday'],
            'password' => $password,
            'ip' => $_SERVER['REMOTE_ADDR'],
			'agent' => $_SERVER['HTTP_USER_AGENT']
        ]);
		//print_r($user);
		Mail::send('emails/register', ['name' => $data['name'], 'password' => $passstr, 'email' => $data['email']], function($m) use ($data)
			{
				$m->from('simon.wayfarer@gmail.com', 'Laravel Development');
				$m->to($data['email'], $data['name'])->subject('Привет, '.$data['name'].'!');
			});
		return $user;
		
    }
}
