<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Auth;
use Image;
use Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use DB;
use App\Comment;
use App\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
	 
    public function index(Request $request)
    {
		$perpage = 25;
		$skip = 0;
		if(isset($request->p) && $request->p > 1){
			$page = $request->p;
			
			$skip = ($page-1)*$perpage;
		}
		$commentsArr = DB::table('comments')->skip($skip)->limit($perpage)->orderBy('created_at', 'desc')->get();
		if(Comment::all()->count() > 0){
			$usersArr = User::all();
			//if(coun){}
			foreach($commentsArr as $commentRes){
				
				$comment['comment'] = $commentRes->comment;
				$comment['parent_id'] = $commentRes->parent_id;
				$date = $commentRes->updated_at;
				
				$comment['filepath'] = $commentRes->filepath;
				$comment['date'] = $date;
				$comment['id'] = $commentRes->id;
				foreach($usersArr as $userRes){
					if($commentRes->user_id == $userRes->id){
						$comment['user_name'] = $userRes->name;
						$comment['user_id'] = $userRes->id;
						
					}
				}
				if($commentRes->parent_id == 0){
					$rootcom[] = $comment;
				}else{
				$comments[] = $comment;}
			}
		}
        return view('home', ['comments' => (isset($comments))?$comments:0, 'rootcom' => (isset($rootcom))?$rootcom:0, 'count' => Comment::all()->count()]);
    }
	public function postComment(Request $request){

		$this->validate($request, [
			'comment' => 'required|string|max:1000',
			
		]);
		$efilereq = explode(',',$request->efile);
		//$efile = substr(, strpos($request->efile, ",")+1);
		$image = base64_decode($efilereq[1]);
        
		$png_url = "uploaded-".md5(time()).".png";
        $path = 'upload/' . $png_url;
		$img = Image::make($image);
		$filesizew = $img->width();
		$filesizeh = $img->height();
		if($filesizew > 100 && $filesizeh > 100){
				$img->resize(500, 500)->save($path);
			}else{
				$response = [
					'status' => 'error',
					'error' => 'Картинка должна быть больше 100х100px'
				];
				return Response::json($response);
			}
		
		if($commentres = Comment::create([
			'user_id' => Auth::user()->id,
			'filepath' => $path,
			'comment' =>  $request->comment,
			'parent_id' => ($request->parent)?$request->parent:0,
		])){
		$response = array(
            'status' => 'success',
            'statuses' => $filesize,
            'msg' => 'Сообщение отправлено',
            'comment' => $commentres,
            'user' => Auth::user()->name,
           
        );
        return Response::json($response);  // <<<<<<<<< see this line
		}else{
			return 'no';
		}
	
	}
}
