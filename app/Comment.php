<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

/**
 * Class Comment
 */
class Comment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['user_id', 'comment', 'parent_id', 'filepath'];
    protected $table = 'comments';
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}